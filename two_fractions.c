//WAP to find the sum of two fractions.
#include<stdio.h>
#include<math.h>
struct fraction
{
    int numerator;
    int denominator;
};
typedef  struct fraction Fraction;

Fraction input(int n)
{
    Fraction retval;
    printf("enter numerator of fraction %d\n: ", n);
    scanf("%d", &retval.numerator);
    printf("enter denominator of fraction %d\n:", n);
    scanf("%d", &retval.denominator);
    return retval;
}
Fraction compute(Fraction f1, Fraction f2)
{
    Fraction sum;
    sum.numerator = (f1.numerator*f2.denominator)+(f2.numerator*f1.denominator);
    sum.denominator = f1.denominator*f2.denominator;
    return sum;
}
int gcd(int a, int b)
{  
 int gcd;
    	for(int i = 1; i <= a && i <= b ; ++i)
    	{
        if(a%i == 0 && b%i == 0)
        gcd = i;
   	}
   	 return gcd;
}
/*int  gcd(int a, int b)
{
	if(b != 0)
{
	return gcd(b, a % b);
}
else
	return a;
}*/

void output(Fraction f1, Fraction f2, Fraction sum, int gcd)
{
    	printf("addition of  %d/%d and %d/%d is %d/%d\n", f1.numerator,f1.denominator, f2.numerator, f2.denominator,sum.numerator/gcd, sum.denominator/gcd);
}
int main()
{
Fraction f1, f2, sum;
int gcd_value;
f1 = input(1);
f2 = input(2);
sum = compute(f1, f2);
gcd_value = gcd(sum.numerator, sum.denominator);
output(f1, f2, sum, gcd_value);
return 0;

}