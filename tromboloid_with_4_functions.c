//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float H()
{
	float h;
	printf("enter height:\n");
	scanf("%f", &h);
	while(h < 0)
		{
		printf("enter a positive number for height:\n");
		scanf("%f", &h);
		}
	return h;
}
float B()
{
	float b;
	printf("enter breadth:\n");
	scanf("%f", &b);
	while(b <= 0)
		{
			printf("enter a number greater than zero for breadth:\n");
			scanf("%f", &b);
		}
	return b;
}

float D()
{
	float d;
	printf("enter depth:\n");
	scanf("%f",&d);
	while(d <= 0)
		{
			printf("enter a number greater than zero for depth:\n");
			scanf("%f", &d);
		}
	return d;
}
float volume(float h, float b, float d)
{
	return ((h*b*d)+(d/b))/3;
}
int main()
{
	float height, breadth, depth, Tvolume;
	height = H();
	breadth = B();
	depth = D();
	Tvolume = volume(height, breadth, depth);
	printf("volume of tromboloid with height:%.2f, breadth:%.2f, depth:%.2f is %.2f\n",height, breadth, depth, Tvolume);
	return 0;
}

