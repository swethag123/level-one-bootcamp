//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct point
{
    float x;
    float y;
};
typedef struct point Point;
Point input(int n)
{
    Point retval;
    printf("enter co-ordinate %d : ", n);
    scanf("%f%f", &retval.x, &retval.y);
    return retval;
}
float compute(Point p1, Point p2)
{
    float distance;
    distance = sqrt(pow(p2.x-p1.x, 2) + pow(p2.y-p1.y, 2));
    return distance;
}
void output(Point p1, Point p2, float distance)
{
    printf("distance between (%.2f,%.2f), (%.2f,%.2f) is %.2f", p1.x, p1.y, p2.x, p2.y, distance);
}
int main()
{
    Point p1,p2;
    float distance;
    p1 = input(1);
    p2 = input(2);
    distance = compute(p1, p2);
    output(p1, p2, distance);
    return 0;
}

