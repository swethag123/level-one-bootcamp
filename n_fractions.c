//WAP to find the sum of n fractions.
#include<stdio.h>
typedef struct
{
	int numerator;
	int denominator;
}Fraction;


int get_n()
{
	int n;
	printf("enter the number of fractions to be added\n");
	scanf("%d", &n);
	return n;
}


void input(int n, Fraction a[n])
{
	Fraction x;
	for(int i = 0; i < n ; i++)
	{
		printf("enter the numerator of fraction %d: ", i + 1);
		scanf("%d", &x.numerator);
		printf("enter the denominator of fraction %d: ", i + 1);
		scanf("%d", &x.denominator);
		a[i] = x;
	}
}
int get_gcd(int a, int b)
{
	int temp;
	while(a!=0)
	{
		temp=a;
		a=b%a;
		b=temp;
	}
	return b;
}
Fraction simplify(Fraction sum)
{
	int gcd;
	gcd = get_gcd(sum.numerator, sum.denominator);
	sum.numerator = sum.numerator/gcd;
	sum.denominator = sum.denominator/gcd;
	return sum;
}
Fraction add_2_fractions(Fraction f1, Fraction f2)
{
	Fraction sum;
	sum.numerator = (f1.numerator * f2.denominator +  f2.numerator * f1.denominator);
	sum.denominator = f1.denominator * f2.denominator;
	sum = simplify(sum);
	return sum;
}
Fraction add_n_fractions(int n, Fraction a[n])
{
	Fraction sum;
	sum = a[0];
	for(int i = 1; i < n; i++)
	{
		sum = add_2_fractions(sum, a[i]);
	}
	return sum;
}
	
void display_sum(int n, Fraction a[n], Fraction sum)
{
	printf("the sum of ");
	for(int i = 0; i < n-1; i++)
	{
		printf("%d/%d + ", a[i].numerator, a[i].denominator);
	}
	printf("%d/%d is: %d/%d.\n", a[n-1].numerator, a[n-1].denominator, sum.numerator, sum.denominator);

}

int main()
{
	int n;
	n = get_n();
	Fraction array[n], sum;
	input(n, array);
	sum = add_n_fractions(n, array);
	display_sum(n, array, sum);
    return 0;
}
	
	
