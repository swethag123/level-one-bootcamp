//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
float Tvolume(float h, float d, float b);
int main()
{
	float h, d, b, volume;
	printf("enter the value of h, d, b :\n");
	scanf("%f%f%f", &h, &d, &b);
	volume = Tvolume(h, d, b);
	printf("total volume of tromboloid with h-%f, d-%f, b-%f is %f\n",h, d, b, volume);
	return 0;
}
float Tvolume(float h, float d, float b)
{
	return ((h * d * b) +(d / b ))/3;
}
