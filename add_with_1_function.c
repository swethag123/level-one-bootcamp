//Write a program to add two user input numbers using one function.
#include<stdio.h>
int add(int, int);
int main()
{
	int a,b,sum;
	printf("enter two numbers to be added\n:");
	scanf("%d%d", &a, &b);
	sum = add(a,b);
	printf("sum of %d and %d is %d\n:", a,b,sum);
	return 0;
}
int add(int num1, int num2)
{
	int result;
	result = num1 + num2;
	return result;
}

