//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input_no_of_elements()
{
	int n;
	printf("enter number of elements of the array :\n");
	scanf("%d", &n);
	return n;
}
void input_array(int n, int a[n])
{
	printf("enter the elements of the array:\n");
	for(int i = 0; i < n; i++)
	{
		scanf("%d", &a[i]);
	}
}
int sum_array(int n, int a[n])
{
	int sum = 0;
	for(int i = 0; i < n; i++)
	{
		sum+=a[i];
	}
    return sum;
}
int output_sum(int n, int a[n], int sum)
{
    int i;
	printf("sum of elements of the array ");
	for(i = 0; i < n-1; i++)
	{
	    printf("%d+", a[i]);
	}
	printf("%d = %d", a[i], sum);
	
}
int main()
{
	int n, sum;
	n = input_no_of_elements();
	int a[n];
	input_array(n, a);
	sum = sum_array(n ,a);
	output_sum(n, a, sum);
return 0;
}

